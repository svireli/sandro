const assert = require('assert');
const Migration = require('../src/migration');
const Loki = require('lokijs');
const Config = require('../config/dev.conf.js');

describe('Migration', function() {
  it('should migrate all collections and data into db', function() {
    const db = new Loki('test.db', {
      autoload: true,
      autoloadCallback: function() {
        const migrator = new Migration(db, Config, console);
        migrator.migrate();
        Config.db.collections.forEach(function(collObj) {
          let collection = db.getCollection(collObj.name);
          assert.notEqual(collection, null);
          collObj.data.forEach(function(item) {
            const found = collection.get(item['$loki']);
            assert.notEqual(found.length, 0);
          });
        });
      },
      autosave: false,
    });
  });
});
