const Georgian = require('./ge');
const English = require('./en');

module.exports = {
  ge: Georgian,
  en: English,
};
