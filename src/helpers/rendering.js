/* eslint no-underscore-dangle: ["error", { "allow": ["_id", "_rev"] }] */
const fs = require('fs');
const Crypto = require('crypto-js');
const puppeteer = require('puppeteer');
const Config = require('../../config');

/**
 * Helper functions for response object
 * @namespace RenderingHelper
 */
const HtmlRenderingHelper = {

  /**
   * Renders requested template in pdf
   * @param {object} template Template object
   * @param {string} checksum Checksum of data input
   * @param {string} html Template Rendered html
   * @param {object} exBrowser Template Rendered html
   * @param {function} cb Callback to do after pdf render
   */
  renderPdf({template, checksum, html, exBrowser, logger, cb}) {
    HtmlRenderingHelper.ensureDirectoriesAreCreated(template, logger, (directoriesErr) => {
      if (directoriesErr) logger.error(`Cant render template: ${template._id}, checksum: ${checksum}, directories are not created`);
      else {
        let options = { waitUntil: 'domcontentloaded' };
        if (template.loadExternalSources) options = { waitUntil: 'load' };
        if (exBrowser) {
          exBrowser.newPage().then(async (page) => {
            await page.setContent(html, options);
            const path = `${Config.storagePath}/${template._id}/${template._rev}/${checksum}.${Config.fileTypes.pdf}`;
            await page.pdf(Object.assign({
              path,
            }, template.options));
            const fileStream = fs.createReadStream(path);
            if (!template.caching) {
              fileStream.on('end', () => {
                fs.unlink(path, (unlinkError) => {
                  if (unlinkError) logger.error(`Failed to remove tmp file after rendering: ${path}`);
                });
              });
            }
            cb(false, path, fileStream);
            await page.close();
          }).catch(err => cb(err));
        } else {
          const browserOptions = { headless: true };
          if (Config.root) browserOptions.args = ['--no-sandbox', '--disable-setuid-sandbox'];
          puppeteer.launch(browserOptions).then(async (browser) => {
            const page = await browser.newPage();
            await page.setContent(html, options);
            const path = `${Config.storagePath}/${template._id}/${template._rev}/${checksum}.${Config.fileTypes.pdf}`;
            await page.pdf(Object.assign({
              path,
            }, template.options));
            await browser.close();
            const fileStream = fs.createReadStream(path);
            if (!template.caching) {
              fileStream.on('end', () => {
                fs.unlink(path, (unlinkError) => {
                  if (unlinkError) logger.error(`Failed to remove tmp file after rendering: ${path}`);
                });
              });
            }
            cb(false, path, fileStream);
          }).catch(err => cb(JSON.stringify(err)));
        }
      }
    });
  },

  /**
   * Saves rendered text in file
   * @param {object} template Template object
   * @param {string} checksum Checksum of data input
   * @param {string} source Source text, data
   * @param {string} fileType Source file type
   * @param {function} cb Callback to do after pdf render
   */
  saveRendered({template, checksum, source, fileType, logger, cb}) {
    HtmlRenderingHelper.ensureDirectoriesAreCreated(template, logger, (err) => {
      if (err) {
        logger.error(`Cant save rendered template: ${template._id}, checksum: ${checksum}, directories are not created`);
        cb(true);
      } else fs.writeFile(HtmlRenderingHelper.getCachedFilePath(template, checksum, fileType), source, { flag: 'wx' }, cb);
    });
  },

  /**
   * Renders requested template in pdf
   * @param {object} template Template object
   * @param {string} checksum Checksum of data input
   * @param {string} fileType Callback to do after pdf render
   * @return {bool}
   */
  checkIfCached(template, checksum, fileType) {
    return fs.existsSync(HtmlRenderingHelper.getCachedFilePath(template, checksum, fileType));
  },
  /**
   * Assembles path of the cached file base on input
   * @param {object} template Template object
   * @param {string} checksum Checksum of data input
   * @param {string} fileType Callback to do after pdf render
   * @return {string}
   */
  getCachedFilePath(template, checksum, fileType) {
    return `${Config.storagePath}/${template._id}/${template._rev}/${checksum}.${fileType}`;
  },

  /**
   * Validates required product
   * @param {object} data object to calculate hash of
   * @return {string} hash
   */
  getObjectCheckSum(data) {
    return Crypto.MD5(JSON.stringify(data)).toString();
  },

  /**
   * Checks and creates directories for template
   * @param {object} template Template object
   * @param {object} logger Logger for logging info
   * @param {function} cb Callback after function
   */
  ensureDirectoriesAreCreated(template, logger, cb) {
    const revisionDir = `${Config.storagePath}/${template._id}/${template._rev}`;
    fs.mkdir(revisionDir, { recursive: true }, (mkdErr) => {
      if (mkdErr) {
        logger.error(`Cant create directory for revision ${revisionDir}`);
        cb(true);
      } else {
        cb(false);
      }
    });
  },
};

module.exports = HtmlRenderingHelper;
