/* eslint no-underscore-dangle: ["error", { "allow": ["_id", "_rev"] }] */
const Hogan = require('hogan.js');
const HttpStatus = require('http-status-codes');
const fs = require('fs');
const del = require('del');
const uuid = require('uuid/v4');
const puppeteer = require('puppeteer');
const async = require('async');
const Config = require('../../config');
const ResponseHelper = require('../helpers/response');
const RenderingHelper = require('../helpers/rendering');

/**
 * Template controller which
 * handles template chaching, rendering with Hogan
 * but uses additional helper for pdf rendering
 * using puppeteer
 */
class TemplateController {
  /**
   * Constructs controller
   * @param {object} templates Couchdb nano object
   * @param {object} partials Couchdb nano object
   * @param {object} config global config object
   * @param {object} logger global logger object
   */
  constructor(templates, partials, config, logger) {
    this.templates = templates;
    this.partials = partials;
    this.config = config;
    this.logger = logger;
    this.compiledTemplates = {};
    this.compiledPartials = null;
    this.reloaderId = null;
    if (this.config.autoCacheReload) {
      const that = this;
      this.reloaderId = setInterval(() => {
        that.logger.info('Auto cache reloader started');
        that.getCompiledTemplates(true, (err) => {
          if (err) that.logger.error(`Finished reloading templates with err: ${err}`);
          that.logger.info('Finished reloading templates');
        });
        that.getCompiledPartials(true, (err) => {
          if (err) that.logger.error(`Finished reloading partials with err: ${err}`);
          that.logger.info('Finished reloading partials');
        });
      }, this.config.cacheReloadInterval * 1000);
    }
  }

  /**
   * Method is used to completely destroy template
   * @param {object} req express request object
   * @param {object} res express response object
   */
  destroy(req, res) {
    const that = this;
    if (req.params && req.params.id) {
      that.templates.get(req.params.id, {
        attachments: false,
      }, (templateGetErr, templateBody) => {
        if (templateGetErr) {
          that.logger.info(`Cant find template with id ${req.params.id}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
          res.status(response.code).send(response);
        } else {
          that.logger.info(`Successfully found template ${req.params.id}`);
          // Remove unneccessary data
          that.templates.destroy(templateBody._id, templateBody._rev, (destroyErr) => {
            if (destroyErr) {
              that.logger.error('Cant remove template');
              const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_RM_TEMPLATE');
              res.status(response.code).send(response);
            } else {
              del([`${Config.storagePath}/${templateBody._id}`]).then(() => that.logger.info('Successfully removed template files'))
                .catch(() => {
                  that.logger.error(`Cant remove folder ${Config.storagePath}/${templateBody._id}`);
                });
              that.logger.info('Successfully removed template');
              const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_REMOVED');
              res.status(response.code).send(response);
              that.logger.info(`Updating cache for template ${templateBody._id} after removal`);
              that.getCompiled(templateBody._id, true, () => {
                that.logger.info(`Removed memory cache for template ${templateBody._id} after destroy successfully`);
              });
            }
          });
        }
      });
    } else {
      that.logger.info('Required parameter id is not present');
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
      res.status(response.code).send(response);
    }
  }

  /**
   * Method is used to get individual template
   * @param {object} req express request object
   * @param {object} res express response object
   */
  get(req, res) {
    const that = this;
    if (req.params && req.params.id) {
      that.templates.get(req.params.id, {
        attachments: false,
      }, (templateGetErr, templateBody) => {
        if (templateGetErr) {
          that.logger.info(`Cant find template with id ${req.params.id}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
          res.status(response.code).send(response);
        } else {
          that.logger.info(`Successfully returned template ${req.params.id}`);
          // Remove unneccessary data
          delete templateBody._attachments;
          that.templates.attachment.get(templateBody._id, Config.templateAttachmentFileName, (attErr, att) => {
            if (attErr) {
              that.logger.info(`Cant find attachment for template id ${req.params.id}`);
              const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
              res.status(response.code).send(response);
            } else {
              templateBody.template = att.toString();
              const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', templateBody);
              res.status(response.code).send(response);
            }
          });
        }
      });
    } else {
      that.logger.info('Required parameter id is not present');
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
      res.status(response.code).send(response);
    }
  }

  /**
   * Method is used for listing template
   * @param {object} req express request object
   * @param {object} res express response object
   */
  list(req, res) {
    const that = this;
    that.templates.find({
      limit: 1000000,
      selector: {},
      fields: ['_id', 'name', 'description', 'category', 'created', 'updated', 'creator', 'editor'],
    }, (listGetErr, listBody) => {
      if (listGetErr) {
        that.logger.error('Cant get list of templates');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_GET_TEMPLATES');
        res.status(response.code).send(response);
      } else {
        that.logger.info('Successfully got templates');
        const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', listBody.docs);
        res.status(response.code).send(response);
      }
    });
  }

  /**
   * Method is used for creating template
   * @param {object} req express request object
   * @param {object} res express response object
   */
  create(req, res) {
    const that = this;
    if (!that.validateForCreating(req, res)) return;
    // We save template object without template string
    // Template string is saved as attachment file
    const templateStr = req.body.template;
    const templateObj = req.body;
    delete templateObj.template;
    delete templateObj._rev;
    if (!templateObj.restore) {
      delete templateObj._id;
      templateObj.created = Date.now();
      templateObj.updated = Date.now();
      templateObj.creator = req.auth.user;
      templateObj.editor = req.auth.user;
    }
    that.templates.insert(templateObj, (templateCreateErr, templateBody) => {
      if (templateCreateErr) {
        that.logger.error('Error while creating template');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_CREATE_TEMPLATE');
        res.status(response.code).send(response);
      } else {
        that.logger.info(`Template ${templateBody.id} created successfully, inserting attachment`);
        that.templates.attachment.insert(templateBody.id, Config.templateAttachmentFileName, templateStr, Config.templateAttachmentExtension, { rev: templateBody.rev }, (attErr) => {
          if (attErr) {
            that.logger.info(`Error while creating attachment for template ${templateBody.id}`);
            const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_CREATE_TEMPLATE');
            res.status(response.code).send(response);
          } else {
            that.logger.info(`successfully inserted and added atachment to template ${templateBody.id}`);
            const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_ADDED', {
              id: templateBody.id,
            });
            res.status(response.code).send(response);
          }
        });
      }
    });
  }

  /**
   * Updates exsisting template
   * @param {object} req express req object
   * @param {object} res express response object
   */
  update(req, res) {
    const that = this;
    if (!that.validateForUpdating(req, res)) return;
    // We save template object without template string
    // Template string is saved as attachment file
    const templateStr = req.body.template;
    delete req.body.template;
    that.templates.get(req.params.id, (templateGetErr, templateBody) => {
      if (templateGetErr) {
        that.logger.info(`Error cant find template with id ${req.params.id}`);
        const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'NOT_FOUND');
        res.status(response.code).send(response);
      } else {
        req.body._id = templateBody._id;
        req.body._rev = templateBody._rev;
        req.body.created = templateBody.created;
        req.body.creator = templateBody.creator;
        req.body.updated = Date.now();
        req.body.editor = req.auth.user;
        that.templates.insert(req.body, (insertionErr, newTemplateBody) => {
          if (insertionErr) {
            that.logger.error(`Error cant update template with id ${req.params.id}`);
            const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_UPDATE_TEMPLATE');
            res.status(response.code).send(response);
          } else {
            // We may not insert attachment but template is changed
            // so here we can remove old revision folder from cache
            del([
              `${Config.storagePath}/${templateBody._id}/${templateBody._rev}`,
            ]).then(() => {
              that.logger.info(`Removed old revision folder ${Config.storagePath}/${templateBody._id}/${templateBody._rev}`);
            }).catch(() => that.logger.error('Del cant remove old revision files'));

            that.templates.attachment.insert(newTemplateBody.id, Config.templateAttachmentFileName, templateStr, Config.templateAttachmentExtension, { rev: newTemplateBody.rev }, (attachmentErr) => {
              if (attachmentErr) {
                that.logger.error(`Error cant update template attachment with id ${req.params.id}`);
                const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_UPDATE_TEMPLATE');
                res.status(response.code).send(response);
              } else {
                that.logger.info(`successfully updated template and atachment ${templateBody._id}`);
                const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_UPDATED', {
                  id: templateBody._id,
                });
                res.status(response.code).send(response);
                that.logger.info(`Updating cache for template ${templateBody._id} after update`);
                that.getCompiled(templateBody._id, true, () => {
                  that.logger.info(`Updated memory cache for template ${templateBody._id} after update successfully`);
                });
              }
            });
          }
        });
      }
    });
  }

  /**
   * Renders template
   * @param {object} req express request object
   * @param {object} res express response object
   */
  render(req, res) {
    const that = this;
    if (!that.validateRequiredParamsForRendering(req, res)) return;
    that.logger.info('Seems like required params are present');
    const forceUncached = req.body.caching === undefined ? false : !req.body.caching;
    that.getCompiledPartials(forceUncached, (partialsErr, partials) => {
      if (!partialsErr) {
        that.logger.info('Got templates from cache');
        that.getCompiled(req.params.id, forceUncached, (templatesErr, template) => {
          if (!templatesErr) {
            that.logger.info('Got templates from cache');
            let checksum;
            if (!forceUncached && template.caching && RenderingHelper.checkIfCached(template, ( checksum = RenderingHelper.getObjectCheckSum(req.body.data) ), req.params.fileType)) {
              that.logger.info(`Seems template ${req.params.id} ${req.params.fileType} is cached`);
              fs.createReadStream(RenderingHelper.getCachedFilePath(template, checksum, req.params.fileType)).pipe(res);
            } else {
              that.logger.info(`Template file is not cached. Rendering text ${req.params.id} ${req.params.fileType}`);
              const renderedText = template.compiled.render(req.body.data, partials);
              if (req.params.fileType === Config.fileTypes.txt) {
                that.logger.info(`Returning requested template text ${req.params.id} ${req.params.fileType}`);
                res.send(renderedText);
                if (template.caching) {
                  that.logger.info(`Cache is enabled, storing requested template text ${req.params.id} ${req.params.fileType} as file`);
                  RenderingHelper.saveRendered({
                    template,
                    checksum: RenderingHelper.getObjectCheckSum(req.body.data),
                    source: renderedText,
                    fileType: req.params.fileType,
                    logger: that.logger,
                    cb: (fileCreateErr) => {
                      if (fileCreateErr) that.logger.error('Cant save rendered file');
                    },
                  });
                }
              } else if (req.params.fileType === Config.fileTypes.pdf) {
                that.getBrowser((browserErr, browser) => {
                  if (browserErr) that.logger.error('Cant get browser from cache, letting renderer create one');
                  RenderingHelper.renderPdf({
                    template,
                    checksum: template.caching ? RenderingHelper.getObjectCheckSum(req.body.data) : uuid(),
                    html: renderedText,
                    browser: browser || null,
                    logger: that.logger,
                    cb: (rendererError, path, fileStream) => {
                      if (template.loadExternalSources) that.logger.info('External sources are enabled, it will slow the rendering');
                      if (!rendererError) {
                        that.logger.info(`File created successfully ${path}`);
                        if (!template.caching) that.logger.info(`Caching is off, file will be deleted after download ${path}`);
                        fileStream.pipe(res);
                      } else {
                        that.logger.error('Browser cant render a template in a file');
                        that.logger.error(rendererError);
                        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_CREATE_FILE');
                        res.status(response.code).send(response);
                      }
                    },
                  });
                });
              } else {
                that.logger.info(`Cant find implementation for template type ${req.params.fileType}`);
                const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'NOT_IMPLEMENTED');
                res.status(response.code).send(response);
              }
            }
          } else {
            that.logger.info('Cant get templates from cache');
            const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
            res.status(response.code).send(response);
          }
        });
      } else {
        that.logger.info('Cant get partials from cache');
        const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
        res.status(response.code).send(response);
      }
    });
  }

  /**
   * Validates request for updating
   * @param {object} req express request object
   * @param {object} res express response object
   * @return {bool} if validation passed
   */
  validateForUpdating(req, res) {
    const that = this;
    if (req.body && req.body.template && req.params && req.params.id) {
      return true;
    }
    that.logger.info('No required parameters passed');
    const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
    res.status(response.code).send(response);
    return false;
  }

  /**
   * Validates request for creating
   * @param {object} req express request object
   * @param {object} res express response object
   * @return {bool} if validation passed
   */
  validateForCreating(req, res) {
    const that = this;
    if (req.body && req.body.template) {
      return true;
    }
    that.logger.info('No required parameters passed');
    const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
    res.status(response.code).send(response);
    return false;
  }

  /**
   * Validates required product
   * @param {object} req express request object
   * @param {object} res express response object
   * @return {bool} if validation passed
   */
  validateRequiredParamsForRendering(req, res) {
    const that = this;
    if (req.body && req.params && req.params.id && req.params.fileType) {
      const keys = Object.keys(Config.fileTypes);
      for (let i = 0; i < keys.length; i += 1) {
        if (keys[i] === req.params.fileType) return true;
      }
    }
    that.logger.info('No required parameters passed');
    const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
    res.status(response.code).send(response);
    return false;
  }

  /**
   * Get cached puppeter browser for Rendering
   * @param {function} cb callback to execute after work is done
   */
  getBrowser(cb) {
    const that = this;
    if (that.browser) {
      that.logger.info('Browser is cached in memory, getting cached');
      cb(false, that.browser);
    } else {
      that.logger.info('Browser is not cached in memory, opening');
      const browserOptions = {};
      if (Config.root) browserOptions.args = ['--no-sandbox', '--disable-setuid-sandbox'];
      puppeteer.launch(browserOptions).then(async (br) => {
        that.browser = br;
        cb(false, br);
      }).catch(err => cb(err));
    }
  }

  /**
   * Checks if template is compiled in memory
   * if it is returns it, if not gets template,
   * compiles it and saves in cache
   * @param {string} templateId Id of template in db
   * @param {bool} forceUncached If this is true we will try to rerender everything
   * @param {function} cb callback to execute after work is done
   */
  getCompiled(templateId, forceUncached, cb) {
    const that = this;
    if (!forceUncached && that.compiledTemplates[templateId]) {
      that.logger.info(`Getting compiled template from memory cache ${templateId}`);
      cb(false, that.compiledTemplates[templateId]);
    } else {
      that.logger.info(`Template ${templateId} is not cached in memory or you dont want cached data`);
      that.templates.get(templateId, (getErr, body) => {
        if (getErr) {
          that.logger.error(`Cant find template ${templateId}, maybe remove, deleting cache`);
          // Perform removal if template is removed
          delete that.compiledTemplates[templateId];
          cb(true);
        } else {
          that.logger.info(`Found template ${templateId}`);
          that.logger.info(`Search for attachment for template ${templateId}`);
          that.templates.attachment.get(templateId, Config.templateAttachmentFileName, (attErr, attBody) => {
            if (!attErr) {
              that.logger.info(`Found attachment, compiling template and saving in cache ${templateId}`);
              body.compiled = Hogan.compile(attBody.toString());
              that.compiledTemplates[templateId] = body;
              cb(false, that.compiledTemplates[templateId]);
            } else {
              that.logger.error(`Cant find attachment for template ${templateId}`);
              cb(true);
            }
          });
        }
      });
    }
  }

  /**
   * List all partial views
   * @param {object} req express request object
   * @param {object} res express response object
   */
  listPartials(req, res) {
    const that = this;
    that.partials.find({
      limit: 1000000,
      selector: {},
      fields: ['_id', 'name', 'description', 'category', 'created', 'updated', 'creator', 'editor'],
    }, (listGetErr, listBody) => {
      if (listGetErr) {
        that.logger.error('Cant get list of partials');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_GET_PARTIALS');
        res.status(response.code).send(response);
      } else {
        that.logger.info('Successfully got partials');
        const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', listBody.docs);
        res.status(response.code).send(response);
      }
    });
  }

  /**
   * Method is used for creating partial
   * @param {object} req express request object
   * @param {object} res express response object
   */
  createPartial(req, res) {
    const that = this;
    if (!that.validateForCreating(req, res)) return;
    // We save partial object without template string
    // Template string is saved as attachment file
    const templateStr = req.body.template;
    const partialBody = req.body;
    delete partialBody.template;
    delete partialBody._rev;
    if (!partialBody.restore) {
      delete partialBody._id;
      partialBody.created = Date.now();
      partialBody.updated = Date.now();
      partialBody.creator = req.auth.user;
      partialBody.editor = req.auth.user;
    }
    that.partials.insert(partialBody, (partialCreateErr, partialDoc) => {
      if (partialCreateErr) {
        that.logger.error('Error while creating partial');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_CREATE_PARTIAL');
        res.status(response.code).send(response);
      } else {
        that.logger.info(`Partial ${partialDoc.id} created successfully, inserting attachment`);
        that.partials.attachment.insert(partialDoc.id, Config.templateAttachmentFileName, templateStr, Config.templateAttachmentExtension, { rev: partialDoc.rev }, (attErr) => {
          if (attErr) {
            that.logger.info(`Error while creating attachment for partial ${partialDoc.id}`);
            const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_CREATE_PARTIAL');
            res.status(response.code).send(response);
          } else {
            that.logger.info(`successfully inserted and added atachment to partial ${partialDoc.id}`);
            const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_ADDED', {
              id: partialDoc.id,
            });
            res.status(response.code).send(response);
            that.logger.info(`Updating cache for partial ${partialDoc.id} after update`);
            that.getCompiledPartials(true, (err) => {
              if (err) that.logger.err('Error happened while updating partials memory cache');
              that.logger.info('successfully updated partials memory cache');
            });
          }
        });
      }
    });
  }

  /**
   * Updates exsisting partial
   * @param {object} req express req object
   * @param {object} res express response object
   */
  updatePartial(req, res) {
    const that = this;
    if (!that.validateForUpdating(req, res)) return;
    // We save partial object without template string
    // Template string is saved as attachment file
    const templateStr = req.body.template;
    delete req.body.template;
    that.partials.get(req.params.id, (partialGetErr, partialDoc) => {
      if (partialGetErr) {
        that.logger.info(`Error cant find partial with id ${req.params.id}`);
        const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'NOT_FOUND');
        res.status(response.code).send(response);
      } else {
        req.body._id = partialDoc._id;
        req.body._rev = partialDoc._rev;
        req.body.created = partialDoc.created;
        req.body.creator = partialDoc.creator;
        req.body.updated = Date.now();
        req.body.editor = req.auth.user;
        that.partials.insert(req.body, (insertionErr, newPartialBody) => {
          if (insertionErr) {
            that.logger.error(`Error cant update partial with id ${req.params.id}`);
            const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_UPDATE_PARTIAL');
            res.status(response.code).send(response);
          } else {
            that.partials.attachment.insert(newPartialBody.id, Config.templateAttachmentFileName, templateStr, Config.templateAttachmentExtension, { rev: newPartialBody.rev }, (attachmentErr) => {
              if (attachmentErr) {
                that.logger.error(`Error cant update partial attachment with id ${req.params.id}`);
                const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_UPDATE_PARTIAL');
                res.status(response.code).send(response);
              } else {
                that.logger.info(`successfully updated partial and atachment ${partialDoc._id}`);
                const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_UPDATED', {
                  id: partialDoc._id,
                });
                res.status(response.code).send(response);
                that.logger.info(`Updating cache for partial ${partialDoc._id} after update`);
                that.getCompiledPartials(true, (err) => {
                  if (err) that.logger.err('Error happened while updating partials memory cache');
                  that.logger.info('successfully updated partials memory cache');
                });
              }
            });
          }
        });
      }
    });
  }

  /**
   * Method is used to completely destroy partial
   * @param {object} req express request object
   * @param {object} res express response object
   */
  destroyPartial(req, res) {
    const that = this;
    if (req.params && req.params.id) {
      that.partials.get(req.params.id, {
        attachments: false,
      }, (partialGetErr, partialBody) => {
        if (partialGetErr) {
          that.logger.info(`Cant find partial with id ${req.params.id}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
          res.status(response.code).send(response);
        } else {
          that.logger.info(`Successfully found partial ${req.params.id}`);
          // Remove unneccessary data
          that.partials.destroy(partialBody._id, partialBody._rev, (destroyErr) => {
            if (destroyErr) {
              that.logger.error('Cant remove partial');
              const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'CANT_RM_PARTIAL');
              res.status(response.code).send(response);
            } else {
              that.logger.info('Successfully removed partial');
              const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_REMOVED');
              res.status(response.code).send(response);
              that.logger.info(`Updating cache for partial ${partialBody._id} after removal`);
              that.getCompiled(partialBody._id, true, () => {
                that.logger.info(`Removed memory cache for template ${partialBody._id} after destroy successfully`);
              });
              // Here we can reload partials cache, but this will not
              // give us performance boost. So we just wait when somebody edits
              // partial so the cache will be reloaded.
            }
          });
        }
      });
    } else {
      that.logger.info('Required parameter id is not present');
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
      res.status(response.code).send(response);
    }
  }

  /**
   * Method is used to get individual partials
   * @param {object} req express request object
   * @param {object} res express response object
   */
  getPartial(req, res) {
    const that = this;
    if (req.params && req.params.id) {
      that.partials.get(req.params.id, {
        attachments: true,
      }, (partialGetErr, partialBody) => {
        if (partialGetErr) {
          that.logger.info(`Cant find partial with id ${req.params.id}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
          res.status(response.code).send(response);
        } else {
          that.logger.info(`Successfully got partial ${req.params.id}`);
          // Remove unneccessary data
          delete partialBody._attachments;
          that.partials.attachment.get(partialBody._id, Config.templateAttachmentFileName, (attErr, att) => {
            if (attErr) {
              that.logger.info(`Cant find attachment for partial id ${req.params.id}`);
              const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
              res.status(response.code).send(response);
            } else {
              partialBody.template = att.toString();
              const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCC_FOUND', partialBody);
              res.status(response.code).send(response);
            }
          });
        }
      });
    } else {
      that.logger.info('Required parameter id is not present');
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'BAD_REQUEST');
      res.status(response.code).send(response);
    }
  }

  /**
   * This functions gets all partials, compiles them and saves in cache.
   * is forceUncached is false and we already have cache we will use it.
   * @param {bool} forceUncached If this is true we will try to rerender everything from db
   * @param {function} cb callback to execute after work is done
   */
  getCompiledPartials(forceUncached, cb) {
    const that = this;
    if (!forceUncached && that.compiledPartials) {
      that.logger.info('Getting compiled partials from memory cache');
      cb(false, that.compiledPartials);
    } else {
      that.logger.info('Partials are not cached in memory or you dont want cached data');
      const localCache = {};
      that.partials.find({
        limit: 1000000,
        selector: {},
        fields: ['_id'],
      }, (listGetErr, listBody) => {
        // We need this variable to save compiled partials
        // Then we just replace reference to this and remove old object
        async.forEach(listBody.docs, (partialDoc, partialCb) => {
          that.partials.attachment.get(partialDoc._id, Config.templateAttachmentFileName, (attErr, attBody) => {
            if (attErr) {
              that.logger.info(`Cant find attachment for partial id ${partialDoc._id}`);
              partialCb();
            } else {
              that.logger.info(`Compiling and saving in memory, attachment for partial id ${partialDoc._id}`);
              localCache[partialDoc._id] = Hogan.compile(attBody.toString());
              partialCb();
            }
          });
        }, () => {
          // Garbadge collector will do the rest
          that.compiledPartials = localCache;
          cb(false, that.compiledPartials);
        });
        if (listGetErr) {
          that.logger.error('Cant get list of partials');
          cb(listGetErr);
        }
      });
    }
  }

  /**
   * This functions gets all partials, compiles them and saves in cache.
   * is forceUncached is false and we already have cache we will use it.
   * @param {bool} forceUncached If this is true we will try to rerender everything from db
   * @param {function} cb callback to execute after work is done
   */
  getCompiledTemplates(forceUncached, cb) {
    const that = this;
    if (!forceUncached && that.compiledTemplates) {
      that.logger.info('Getting compiled templates from memory cache');
      cb(false, that.compiledTemplates);
    } else {
      that.logger.info('Templates are not cached in memory or you dont want cached data');
      that.templates.find({
        limit: 1000000,
        selector: {},
        fields: ['_id'],
      }, (listGetErr, listBody) => {
        if (listGetErr) {
          that.logger.error('Cant get list of templates');
          cb(listGetErr);
        }

        async.forEach(listBody.docs, (templateDoc, templateCb) => {
          that.getCompiled(templateDoc._id, forceUncached, () => {
            templateCb();
          });
        }, () => {
          cb(false, that.compiledTemplates);
        });
      });
    }
  }
}

module.exports = TemplateController;
