/* eslint no-underscore-dangle: ["error", { "allow": ["_id", "_rev"] }] */
const HttpStatus = require('http-status-codes');
const ResponseHelper = require('../helpers/response');

/**
 * User controller which
 * handles user managements
 */
class UserController {
  /**
   * Constructs controller
   * @param {object} users Couchdb users collection object
   * @param {object} config global config object
   * @param {object} logger global logger object
   */
  constructor(users, config, logger) {
    this.users = users;
    this.config = config;
    this.logger = logger;
  }

  /**
   * Adds requested user in collection
   * @param {object} req express request object
   * @param {object} res express response object
   */
  add(req, res) {
    const that = this;
    if (req.body && req.body.user && req.body.password) {
      that.users.find({
        selector: {
          user: { $eq: req.body.user },
        },
        fields: ['user'],
        limit: 1,
      }, (findErr, body) => {
        if (findErr) {
          that.logger.error(`Can't search users, user: ${req.body.user}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'SYSTEM_ERROR');
          res.status(response.code).send(response);
        } else if (body.docs.length === 0) {
          that.logger.info(`Can't find user in users, user: ${req.body.user}, adding it`);
          that.users.insert({
            user: req.body.user,
            password: req.body.password,
            role: req.body.role,
            created: Date.now(),
            creator: req.auth.user,
          }, (insertErr) => {
            if (insertErr) {
              that.logger.error(`Can't insert user in users, user: ${req.body.user}`);
              const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'SYSTEM_ERROR');
              res.status(response.code).send(response);
            } else {
              that.logger.info(`Inserted user in users, user: ${req.body.user}`);
              const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCCESSFULLY_ADDED');
              res.status(response.code).send(response);
            }
          });
        } else {
          that.logger.info(`User already exsists in users, user: ${req.body.user}`);
          const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'ALREADY_REGISTERED');
          res.status(response.code).send(response);
        }
      });
    } else {
      that.logger.info('Required data is not present');
      const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'SYSTEM_ERROR');
      res.status(response.code).send(response);
    }
  }

  /**
   * Remove requested user from collection
   * @param {object} req express request object
   * @param {object} res express response object
   */
  remove(req, res) {
    const that = this;
    const userId = req.params ? req.params.id : null;
    if (!userId) {
      const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCCESSFULLY_REMOVED');
      res.status(response.code).send(response);
    }
    that.users.get(userId, (findErr, userBody) => {
      if (findErr) {
        that.logger.info(`Can't find user: ${userId} in db`);
        const response = ResponseHelper.create(false, HttpStatus.BAD_REQUEST, 'USR_NOT_FOUND');
        res.status(response.code).send(response);
      } else {
        that.logger.info(`Found user: ${userId} in db, removing`);
        that.users.destroy(userBody._id, userBody._rev, (destroyErr) => {
          if (destroyErr) {
            that.logger.error(`Can't remove user: ${userId} in db`);
            const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'SYSTEM_ERROR');
            res.status(response.code).send(response);
          } else {
            that.logger.info(`Successfully removed user: ${userId} from db`);
            const response = ResponseHelper.create(true, HttpStatus.OK, 'SUCCESSFULLY_REMOVED');
            res.status(response.code).send(response);
          }
        });
      }
    });
  }

  /**
   * Get all users from collection
   * @param {object} req express request object
   * @param {object} res express response object
   */
  list(req, res) {
    const that = this;
    that.users.find({
      limit: 1000000,
      selector: {},
      fields: ['_id', 'user', 'role', 'created', 'creator'],
    }, (err, body) => {
      if (err) {
        that.logger.error('Cant list users');
        const response = ResponseHelper.create(false, HttpStatus.INTERNAL_SERVER_ERROR, 'SYSTEM_ERROR');
        res.status(response.code).send(response);
      } else {
        that.logger.info('Successfully listed users');
        const response = ResponseHelper.create(true, HttpStatus.OK, '', body.docs);
        res.status(response.code).send(response);
      }
    });
  }
}

module.exports = UserController;
