const async = require('async');
/**
 * Migrates all collections from config
 * with initial data into couchbase.
 */
class Migration {
  /**
   * Construct a migration class object
   * @param {object} db Couchdb db object
   * @param {object} config Global config object
   * @param {object} logger Winston logger object
   */
  constructor(db, config, logger) {
    this.db = db;
    this.config = config;
    this.logger = logger;
  }

  /**
   * Migrates all data and collections
   * @param {function} finishCb Callback function
   */
  migrate(finishCb) {
    const that = this;
    async.forEach(that.config.db.collections, (collObj, collCb) => {
      that.db.get(collObj.name, (collErr) => {
        if (collErr) {
          that.logger.debug(`Seems like collection: ${collObj.name} doest exsists`);
          that.db.create(collObj.name, () => {
            that.logger.debug(`Created collection: ${collObj.name}`);
            const coll = that.db.use(collObj.name);
            async.forEach(collObj.data, (item, dataCb) => {
              coll.insert(item, () => {
                that.logger.debug(`Inserting item in collection: ${collObj.name}`);
                dataCb();
              });
            }, () => collCb());
          });
        } else {
          collCb();
        }
      });
    }, finishCb);
  }
}
module.exports = Migration;
