module.exports = {
  root: true,
  renderAuth: false,
  autoCacheReload: false,
  cacheReloadInterval: 600,
  storagePath: './storage',
  templateAttachmentFileName: 'template.html',
  templateAttachmentExtension: 'text/html',
  fileTypes: {
    pdf: 'pdf',
    txt: 'txt',
    doc: 'docx',
    png: 'png',
  },
  logger: {
    file: 'logs/file.log',
    level: 'info',
  },
  server: {
    port: 3000,
    maxRequestSize: '6mb',
  },
  db: {
    url: 'http://admin:123@localhost:5984',
    collections: [{
      name: 'users',
      data: [{
        user: 'admin',
        password: '123',
        role: 'admin',
        created: 1,
        creator: 'system',
      }],
    }, {
      name: 'templates',
      data: [],
    }, {
      name: 'partials',
      data: [],
    }],
  },
};
